<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id [bigint unsigned]
 * @property string $username [varchar(255)] Имя пользователя // TODO:изменить тип данных в миграции
 * @property string $email [varchar(255)] Почта // TODO:изменить тип данных в миграции
 * @property string $password [varchar(255)] Хэш пароля
 */
class Customer extends Model
{
    use HasFactory;

    /** Название таблицы
     * @var string
     */
    protected $table = 'customers';
}
