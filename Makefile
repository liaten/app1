build:
	docker compose up --build -d

up:
	docker compose up -d

down:
	docker compose down

dce:
	docker compose exec $(ARGS)

cli-init:
	make dce ARGS="php-cli $(ARGS)"

fpm-init:
	make dce ARGS="php-fpm $(ARGS)"

cli:
	make cli-init ARGS="bash"

fpm:
	make fpm-init ARGS="bash"

npm-build:
	make dce ARGS="node npm run build"

npm-install:
	make dce ARGS="node npm install"

npm-dev:
	make dce ARGS="node npm run dev"

tinker:
	make dce ARGS="-u 0 php-cli php artisan tinker"

artisan-root:
	make fpm-init ARGS="php artisan $(ARGS)"

artisan-user:
	make cli-init ARGS="php artisan $(ARGS)"

inspire:
	make artisan-root ARGS="inspire"

migrate:
	make artisan-user ARGS="migrate --isolated"

migrate-status:
	make artisan-user ARGS="migrate:status"

migrate-rollback:
	make artisan-user ARGS="migrate:rollback --step=1"

config-cache:
	make artisan-user ARGS="config:cache"

create-controller:
	make artisan-user ARGS="make:controller $(NAME)"

create-migration:
	make artisan-user ARGS="make:migration $(NAME)"

create-view:
	make artisan-user ARGS="make:view $(NAME)"

create-model:
	make artisan-user ARGS="make:model $(NAME)"

create-factory:
	make artisan-user ARGS="make:factory $(NAME)"
