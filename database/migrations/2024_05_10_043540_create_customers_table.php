<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    private const TABLE_NAME = 'customers';

    /** Создание таблицы с покупателями */
    public function up(): void
    {
        try {
            Schema::create(self::TABLE_NAME, static function (Blueprint $table) {
                $table->id();
                $table->string('username');
                $table->string('email')->unique();
                $table->string('password');
            });
        } catch (Throwable $e) {
            $this->down();
            DB::delete("delete from migrations where migration = '2024_05_10_043540_create_customers_table'");
            throw new RuntimeException("Ошибка создания таблицы {$e->getMessage()}");
        }
    }

    /** Удаление таблицы */
    public function down(): void
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
};
